-- Peak of Infection rate for each country

SELECT
	location AS CountryName,
	population AS TotalPopulation,
	MAX(total_cases) AS TotalInfection,
	MAX(total_cases / population) * 100 AS HighestInfectionRate
FROM dbo.CovidDeaths
GROUP BY location, population
ORDER BY HighestInfectionRate DESC