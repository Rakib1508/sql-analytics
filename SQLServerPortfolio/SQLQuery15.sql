-- Using CTE to find global counts on vaccination

WITH cte_table AS
(
	SELECT
		cd.date AS Date,
		cv.new_vaccinations AS DailyVaccination
		FROM dbo.CovidDeaths cd
		JOIN dbo.CovidVaccination cv
			ON cd.location = cv.location AND cd.date = cv.date
		WHERE cd.continent IS NOT NULL
)

SELECT
	CAST(date AS DATE) AS Date,
	SUM(CONVERT(INT, DailyVaccination)) AS GlobalDailyVaccination
FROM cte_table
WHERE DailyVaccination IS NOT NULL
GROUP BY date
ORDER BY date