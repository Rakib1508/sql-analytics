-- Explore two database tables

-- Covid Deaths records
SELECT *
FROM dbo.CovidDeaths
ORDER BY location, date

-- Covid Vaccination records
SELECT *
FROM dbo.CovidVaccination
ORDER BY location, date