-- Create a view to store rolling vaccination counts on daily basis

-- run the commented query to confirm after executing CREATE VIEW statement
-- RUN ONLY THE SELECT STATEMENT BY MARKING THAT
-- OR TAKE THAT IN A SEPARATE SCRIPT

CREATE VIEW VaccinationPercentage AS
SELECT
	cd.continent AS Continent,
	cd.location AS Country,
	cd.date AS Date,
	cd.population AS Population,
	cv.new_vaccinations AS DailyVaccinationCount,
	SUM(CONVERT(INT, cv.new_vaccinations))
		OVER (PARTITION BY cd.location
		ORDER BY cd.location, cd.date) AS RollingVaccinationCount
FROM dbo.CovidDeaths cd
JOIN dbo.CovidVaccination cv
	ON cd.location = cv.location AND cd.date = cv.date
WHERE cd.continent IS NOT NULL


--SELECT *
--FROM dbo.VaccinationPercentage