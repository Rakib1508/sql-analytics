-- Global numbers on COVID-19 daily death rate
-- Total population vs Number of deaths per day basis

SELECT
	date AS Date,
	TotalCount AS DailyInfection,
	TotalDeaths AS DailyDeaths,
	(TotalDeaths / TotalCount) * 100 AS DailyDeathRate
	FROM 
	(
		SELECT
			date,
			SUM(new_cases) AS TotalCount,
			SUM(CAST(new_deaths AS INT)) AS TotalDeaths
		FROM dbo.CovidDeaths
		GROUP BY date
	) AS StatTable
WHERE TotalCount <> 0
ORDER BY date