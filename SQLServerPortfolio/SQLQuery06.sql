-- Sorted Total Infections for each country
-- With Infection Rate

SELECT
	location AS CountryName,
	population AS TotalPopulation,
	MAX(total_cases) AS TotalInfection,
	MAX(total_cases / population) * 100 AS HighestInfectionRate
FROM dbo.CovidDeaths
WHERE continent IS NOT NULL
GROUP BY location, population
ORDER BY TotalInfection DESC