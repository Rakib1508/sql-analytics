-- Global Stats on COVID-19

SELECT *, (TotalDeaths / TotalCases) * 100 AS DeathRate
FROM (SELECT
	SUM(new_cases) AS TotalCases,
	SUM(CAST(new_deaths AS INT)) AS TotalDeaths
	FROM dbo.CovidDeaths
) AS records