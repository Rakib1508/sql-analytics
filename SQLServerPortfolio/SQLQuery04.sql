-- Explore population vs total cases for each country
-- daywise percentage of infection against country population
SELECT 
	location AS CountryName,
	date AS Date,
	population AS TotalPopulation,
	total_cases AS CaseCount,
	(total_cases / population) * 100 AS InfectionRate
FROM dbo.CovidDeaths
ORDER BY location, date