-- Using CTE to find rolling rate of vaccination

WITH cte_table (
	Location, Date, Population, DailyVaccination, RollingVaccination
)
AS (
	SELECT
		cd.location AS Country,
		cd.date AS Date,
		cd.population AS Population,
		cv.new_vaccinations AS DailyVaccination,
		SUM(CONVERT(INT, cv.new_vaccinations)) OVER (
			PARTITION BY cd.location
			ORDER BY cd.location, cd.date
		) AS RollingVaccination
		FROM dbo.CovidDeaths cd
		JOIN dbo.CovidVaccination cv
			ON cd.location = cv.location AND cd.date = cv.date
		WHERE cd.continent IS NOT NULL
)

SELECT
	*, (RollingVaccination / Population) * 100 AS VaccinationPercentile
FROM cte_table
WHERE DailyVaccination IS NOT NULL
ORDER BY 1, 2