-- Get rolling sum for number of vaccinated people per country

-- use the commented AND condition to get only records for days
-- when people did get vaccinated and ignore days without any vaccination

SELECT
	cd.continent AS Continent,
	cd.location AS Country,
	cd.date AS Date,
	cd.population AS Population,
	cv.new_vaccinations AS DailyVaccination,
	SUM(CONVERT(INT, cv.new_vaccinations))
		OVER (PARTITION BY cd.location
			ORDER BY cd.location, cd.date
		) AS RollingVaccinationCount
FROM dbo.CovidDeaths cd
JOIN dbo.CovidVaccination cv
	ON cd.location = cv.location
	AND cd.date = cv.date
WHERE cd.continent IS NOT NULL
-- AND cv.new_vaccinations IS NOT NULL
ORDER BY 2, 3