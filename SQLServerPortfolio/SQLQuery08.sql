-- Fatality Rate against population for each country
-- with Peak of Infection rate

WITH cte_records AS (
	SELECT
		location AS Continent,
		population AS TotalPopulation,
		MAX(CAST(total_deaths AS INT)) AS TotalDeaths,
		MAX(total_cases / population) * 100 AS HighestInfectionRate
	FROM dbo.CovidDeaths
	WHERE continent IS NULL AND location NOT IN ('World', 'International')
	GROUP BY location, population
)

SELECT *,
	(TotalDeaths / TotalPopulation) * 100 AS FatalityRate
FROM cte_records
ORDER BY TotalDeaths DESC