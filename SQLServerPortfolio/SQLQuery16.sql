-- Using temp table to find rolling rate of vaccination

-- use the commented WHERE condition to get only records for days
-- when people did get vaccinated and ignore days without any vaccination
-- IF YOU DO THIS, MAKE SURE TO COMMENT OUT THE FIRST WHERE CONDITION

DROP TABLE IF EXISTS #VaccinationPercentage
CREATE TABLE #VaccinationPercentage
(
	Country NVARCHAR(255),
	Date DATETIME,
	Population NUMERIC,
	NewVaccination NUMERIC,
	RollingCount NUMERIC,
)

INSERT INTO #VaccinationPercentage
SELECT
	cd.location, cd.date, cd.population, cv.new_vaccinations,
	SUM(CONVERT(INT, cv.new_vaccinations))
		OVER (PARTITION BY cd.location
		ORDER BY cd.location, cd.date) AS RollingVaccinationCount
FROM dbo.CovidDeaths cd
JOIN dbo.CovidVaccination cv
	ON cd.location = cv.location AND cd.date = cv.date
WHERE cd.continent IS NOT NULL
ORDER BY 2, 3

SELECT *, (RollingCount / Population) * 100
FROM #VaccinationPercentage
WHERE RollingCount IS NOT NULL
-- WHERE NewVaccination IS NOT NULL
ORDER BY Country, Date