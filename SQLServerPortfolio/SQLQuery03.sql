-- Explore total cases vs total deaths in Bangladesh
-- daywise percentage of death against number of infected people

SELECT 
	location AS CountryName,
	date AS Date,
	total_cases AS CaseCount,
	total_deaths AS DeathCount,
	(total_deaths / total_cases) * 100 AS Fatality
FROM dbo.CovidDeaths
WHERE location = 'Bangladesh'
ORDER BY date