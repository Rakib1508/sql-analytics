-- Extract necessary columns

SELECT
	location AS CountryName,
	population AS Population,
	date AS Date,
	total_cases AS CaseCount,
	new_cases AS DailyCount,
	total_deaths AS DeathCount
FROM dbo.CovidDeaths
ORDER BY location, date