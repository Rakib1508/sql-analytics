SELECT
	location,
	population,
	date,
	MAX(total_cases) AS HighestInfectionCount,
	Max(total_cases / population) * 100 AS PercentPopulationInfected
FROM dbo.CovidDeaths
--WHERE location LIKE '%bangladesh%'
GROUP BY location, population, date
ORDER BY PercentPopulationInfected DESC