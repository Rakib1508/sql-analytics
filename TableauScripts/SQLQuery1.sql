SELECT
	SUM(new_cases) AS total_cases,
	SUM(cast(new_deaths AS INT)) AS total_deaths,
	SUM(cast(new_deaths AS INT))/SUM(New_Cases)*100 AS DeathPercentage
FROM dbo.CovidDeaths
--WHERE location LIKE '%bangladesh%'
WHERE continent IS NOT NULL 
--GROUP BY date
ORDER BY 1, 2