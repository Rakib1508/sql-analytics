SELECT
	location,
	SUM(cast(new_deaths AS INT)) AS TotalDeathCount
FROM dbo.CovidDeaths
--WHERE location LIKE '%bangladesh%'
WHERE continent IS NULL 
	AND location NOT IN ('World', 'European Union', 'International')
GROUP BY location
ORDER BY TotalDeathCount DESC