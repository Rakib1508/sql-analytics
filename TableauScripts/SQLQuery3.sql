SELECT
	location,
	population,
	MAX(total_cases) AS HighestInfectionCount, 
	Max(total_cases / population) * 100 AS PercentPopulationInfected
FROM dbo.CovidDeaths
--WHERE location LIKE '%bangladesh%'
GROUP BY location, population
ORDER BY PercentPopulationInfected DESC