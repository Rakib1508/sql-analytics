# Data Analytics with SQL

This repository consists data exploration and analytics on COVID-19 dataset using SQL for a few examples. I also have Tableau dashboard on the same data made and uploaded on the Tableau community. The links for dataset and data dashboards will be provided below. Let's first look at all the examples we have for SQL:

1. Explore data from `CovidDeaths` and `CovidVaccination` database tables ordered by country name and date.

2. View all data from `CovidDeaths` by extracting only the required columns. Perform sorting by country name and date.

3. As of on 22 September 2021, find the daily `Total Cases vs Total Deaths` data for COVID-19 situation in Bangladesh with ordering by date. Also find the daily rate of death with respect to positive tests.

4. As of on 22 September 2021, find the daily `Total Cases vs Population` data for COVID-19 situation in each country with ordering by infection rate. Also find the daily infection rate with respect to total population of correspoding country.

5. Generate the peak of fatality rate due to COVID-19 in each country and order them with highest infection rate first.

6. Generate the total number of infection due to COVID-19 in each country and order them with highest infection count first. Exclude any non-suitable data.

7. Generate the peak of fatality rate due to COVID-19 in each country using `CTE` and order them with highest infection rate first. Also find the overall fatality rate and sort by total death count with highest being at the top.

8. Generate the peak of fatality rate due to COVID-19 in each continent using `CTE` and order them with highest infection rate first. Also find the overall fatality rate and sort by total death count with highest being at the top.

9. Find the global daily death rate along with daily death count and daily test count. Measure the rate carefully so that there is no `Zero Division Error` and sort by the date.

10. Find the global daily death rate along with daily death count and daily test count. Measure the rate carefully so that there is no `Zero Division Error` and sort by the daily death count.

11. Find the global daily death rate along with daily death count and daily test count. Measure the rate carefully so that there is no `Zero Division Error` and sort by the daily infection count.

12. Generate overall global stats on COVID-19 data, find total infection, total death and rate of death worldwide.

13. Generate rolling count of vaccination for each country with respect to the corresponding population and sort the result first by country then by date. Also find the percentile of vaccinated population.

14. Use `CTE` method to generate rolling count of vaccination for each country and rate of vaccinated population with respect to the corresponding population and sort the result first by country followed by date. Also find the percentile of vaccinated population.

15. Use `CTE` method to generate daily count of vaccination worldwide sorted by the date. Exclude days where there was no vaccination.

16. Use `temp table` method to generate rolling count of vaccination for each country and rate of vaccinated population with respect to the corresponding population and sort the result first by country followed by date. Also find the percentile of vaccinated population.

17. Create a view to store rolling count of vaccination for each country and rate of vaccinated population with respect to the corresponding population and sort the result first by country followed by date.

Collect the COVID-19 dataset from [`Statistics and Research - Coronavirus (COVID-19) Deaths`](https://ourworldindata.org/covid-deaths). See the visualizations provided on the page to get a brief idea about what you are going to do. Download the data as CSV file and import it in a database following the guidelines on [`MicroSoft's Official Website`](https://docs.microsoft.com/en-us/sql/relational-databases/import-export/import-flat-file-wizard?view=sql-server-ver15).

Check out the Tableau Dashboard for this dataset on [`Tableau Public Platform`](https://public.tableau.com/app/profile/muhammad.abdur.rakib/viz/COVID-19Dashboard_16270367219450/Dashboard1). This dashboard is public, you can download if you want.
